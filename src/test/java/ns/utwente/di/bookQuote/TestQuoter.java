package ns.utwente.di.bookQuote;

import nl.utwente.di.bookQuote.Quoter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * Tests the quoter.
 */
public class TestQuoter {
    /**
     * Tests the first book.
     */
    @Test
    public void testBook1() throws Exception {
        final Quoter quoter = new Quoter();
        final double price = quoter.getBookPrice("1");
        Assertions.assertEquals(10.0, price, 0.0, "Price of book 1.");
    }
}
