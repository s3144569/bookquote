package nl.utwente.di.bookQuote;

/**
 * A book quoter.
 */
public class Quoter {
    /**
     * Quotes a book based on its isbn.
     *
     * @param isbn The isbn.
     * @return The price.
     */
    public double getBookPrice(final String isbn) {
        return switch (isbn) {
            case "1" -> 10.0;
            case "2" -> 45.0;
            case "3" -> 20.0;
            case "4" -> 35.0;
            case "5" -> 50.0;
            default -> 0.0;
        };
    }
}
